(function (DistrictPicker, $, undefined) {

    
    DistrictPicker.districtMap = {
        "1": [
            "Baraga",
            "Dickinson",
            "Gogebic",
            "Houghton",
            "Iron",
            "Keweenaw",
            "Marquette",
            "Menominee",
            "Ontonagon"
        ],
        "2": [
            "Alger",
            "Chippewa",
            "Delta",
            "Luce",
            "Mackinac",
            "Schoolcraft"
        ],
        "3": [
            "Antrim",
            "Benzie",
            "Charlevoix",
            "Cheboygan",
            "Emmet",
            "Grand Traverse",
            "Kalkaska",
            "Leelanau",
            "Manistee"
        ],
        "4": [
            "Alcona",
            "Alpena",
            "Arenac",
            "Crawford",
            "Iosco",
            "Montmorency",
            "Ogemaw",
            "Oscoda",
            "Otsego",
            "Presque Isle",
            "Roscommon"
        ],
        "5": [
            "Lake",
            "Mason",
            "Muskegon",
            "Newaygo",
            "Oceana"
        ],
        "6": [
            "Clare",
            "Gladwin",
            "Isabella",
            "Mecosta",
            "Missaukee",
            "Osceola",
            "Wexford"
        ],
        "7": [
            "Allegan",
            "Kent",
            "Ottawa"
        ],
        "8": [
            "Barry",
            "Clinton",
            "Eaton",
            "Gratiot",
            "Ingham",
            "Ionia",
            "Montcalm"
        ],
        "9": [
            "Bay",
            "Genesee",
            "Midland",
            "Saginaw",
            "Shiawassee"
        ],
        "10": [
            "Huron",
            "Lapeer",
            "Sanilac",
            "St. Clair",
            "Tuscola"
        ],
        "11": [
            "Macomb",
            "Oakland",
            "Wayne"
        ],
        "12": [
            "Hillsdale",
            "Jackson",
            "Lenawee",
            "Livingston",
            "Monroe",
            "Washtenaw"
        ],
        "13": [
            "Berrien",
            "Branch",
            "Calhoun",
            "Cass",
            "Kalamazoo",
            "St. Joseph",
            "Van Buren"
        ]
    };

    DistrictPicker.metadistrictMap = {
        "Upper Peninsula": [1, 2],
        "Lower Peninsula": [3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13]
    };

}(window.DistrictPicker = window.DistrictPicker || {}, jQuery))
