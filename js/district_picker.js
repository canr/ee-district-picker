(function (DistrictPicker, $, undefined) {
    /* ********
     * Elements
     * ********/
    var districtCheckboxes$ = $('input.district');
    var metadistrictCheckboxes$ = $('input.metadistrict');
    var statewideCheckbox$ = $('input.statewide');
    var metaCheckboxes$ = $('#metacheckboxes input');

    // Districts
    var districts = DistrictPicker.districtMap;
    var metadistricts = DistrictPicker.metadistrictMap;

    // Tree
    DistrictPicker.tree = null;


    /**
     * A node in a tree of checkboxes.
     * 
     * @constructor
     * @param {jQuery} checkbox$ jQuery wrapped checkbox element which this node wraps.
     */
    var Node = function (checkbox$) {
        this.checkbox$ = checkbox$;
        this.parent = null;
        this.children = [];

        this.checkbox$.on('click.DistrictPicker', this.onClick.bind(this));
    }
    Node.prototype = {
        /**
         * Click event handler.
         *
         * Sets the states of this checkbox, its children, and its parents.
         */
        onClick: function () {
            // Set children's states
            this.setChecked(this.checkbox$.prop('checked'));

            // Set parents' states
            this.setParentState(this.checkbox$.prop('checked'), this.checkbox$.prop('indeterminate'));
        },

        /**
         * Sets whether this checkbox and its children are checked.
         *
         * @param {bool} checked Whether the checkbox is checked.
         */
        setChecked: function (checked) {
            // Set checked state
            this.checkbox$.prop('checked', checked);
            this.checkbox$.prop('indeterminate', false);

            // Recurse
            for (var i in this.children) {
                var child = this.children[i];

                child.setChecked(checked);
            }
        },

        /**
         * Determines whether all child checkboxes are in the given state.
         *
         * @param  {bool} checked Checked state in which all checkboxes must be.
         * @return {bool} Whether this checkbox and its children are in the given checked state.
         */
        hasCheckedState: function (checked) {

            // Does this node have the specified state?
            if (this.checkbox$.prop('checked') !== checked) {
                return false;
            }

            // Do its children have the specified state?
            if (this.children.length !== 0) {

                for (var i = 0; i < this.children.length; i++) {
                    var child = this.children[i];

                    // If any one checkbox is in the wrong state, false
                    if (!child.hasCheckedState(checked)) {
                        return false;
                    }
                }
            }

            return true;
        },

        /**
         * Determines the state of this parent node and its parent nodes.
         * 
         * @param {bool} checked Whether this node should be checked.
         * @param {bool} indeterminate Whether this node's child was indeterminate.
         */
        setParentState: function (checked, indeterminate) {
            // Temporarily set the checked state
            this.checkbox$.prop('checked', checked);

            // Is the checkbox in an indeterminate state?
            var indeterminate = !this.hasCheckedState(checked);
            if (!indeterminate) {
                this.checkbox$.prop('checked', checked);
                this.checkbox$.prop('indeterminate', false);
            }
            else {
                this.checkbox$.prop('checked', false);
                this.checkbox$.prop('indeterminate', true);
            }

            if (this.parent !== null) {
                this.parent.setParentState(checked, indeterminate);
            }
        },

        /**
         * Adds a child node to this node.
         * 
         * @param {Node} node A node to be appended to this node.
         */
        addChild: function (node) {
            node.parent = this;
            this.children.push(node);
        },

        unbind: function () {
            // Remove click event
            this.checkbox$.off('click.DistrictPicker');

            // Recurse
            for (var i in this.children) {
                var child = this.children[i];

                child.unbind();
            }
        }

    }


    /**
     * A tree of checkboxes.
     *
     * @param {jQuery} checkboxes$ A jQuery object containing a set of checkbox objects.
     */
    var Tree = function (checkboxes$) {
        // References
        this.root = null; /**< Root checkbox Node of the tree. This is the All checkbox. */
        this.leaves = []; /**< End checkbox Nodes of the tree. These are the county checkboxes. */
        this.checkboxes$ = checkboxes$; /**< The jQuery county checkbox objects we're toggling. */

        // Make sure that the checkboxes have the appropriate classes and attributes
        this.setCountyCheckboxData();

        // Add nodes to tree
        this.buildTree();

        // Set checkboxes
        this.setMetaCheckboxes();
    }
    Tree.prototype = {
        /**
         * Adds district-picker county attributes and classes to the checkbox elements.
         */
        setCountyCheckboxData: function () {
            this.checkboxes$.each( function () {
                if (!$(this).hasClass('district-picker')) {
                    // County name
                    var countyName = $(this).parent()[0].textContent.trim();
                    $(this).attr('countyname', countyName);
                    $(this).addClass('district-picker county');
                }
            });
        },

        /**
         * Constructs the tree of checkbox Node objects based on the Tree's target checkboxes.
         */
        buildTree: function () {
            // Set root
            this.root = new Node(statewideCheckbox$);

            // Metadistricts
            for (var metadistrictName in metadistricts) {
                var metadistrict = metadistricts[metadistrictName];
                var metadistrictCheckbox$ = metadistrictCheckboxes$.filter('[value="'+metadistrictName+'"]');

                // Add node to tree
                var metadistrictNode = new Node(metadistrictCheckbox$);
                this.root.addChild(metadistrictNode);

                // Districts
                for (var metadistrictNum in metadistrict) {
                    var districtNum = metadistrict[metadistrictNum];
                    var district = districts[districtNum];
                    var districtCheckbox$ = districtCheckboxes$.filter('[value="'+districtNum+'"]');

                    // Add node to tree
                    var districtNode = new Node(districtCheckbox$);
                    metadistrictNode.addChild(districtNode);

                    // Set metadistrict on district checkboxes
                    districtCheckbox$.attr('metadistrict', metadistrictName);

                    // Counties
                    for (var i in district) {
                        var countyName = district[i];

                        // Set metadistrict and district on county checkboxes
                        var checkbox$ = this.checkboxes$.filter('[countyname="'+countyName+'"]');
                        if (checkbox$.length !== 0) {
                            checkbox$.attr('metadistrict', metadistrictName);
                            checkbox$.attr('district', districtNum);

                            // Add node to tree
                            var node = new Node(checkbox$);
                            districtNode.addChild(node);
                            this.leaves.push(node);
                        }
                    }
                }
            }
        },

        /**
         * Set the district, peninsula, and statewide checkbox states based on the states of the county checkboxes.
         */
        setMetaCheckboxes: function () {
            // Clear old states
            this.clearMetaCheckboxes();

            // Set state
            for (var i = 0; i < this.leaves.length; i++) {
                var leaf = this.leaves[i];
                var checked = leaf.checkbox$.prop('checked');

                leaf.setParentState(checked, false);
            }
        },

        /**
         * Set the district, peninsula, and statewide checkboxes to be unchecked and determinate.
         */
        clearMetaCheckboxes: function () {
            metaCheckboxes$.each( function () {
                $(this).prop('checked', false);
                $(this).prop('indeterminate', false);
            });
        },

        /**
         * Teardown functionality used to free memory and cancel event handlers before a Tree is released.
         */
        destroy: function () {
            // Recursively unbind click handlers
            this.root.unbind();

            // Dereference leaves
            this.leaves = null;

            // Dereference root
            this.root = null;

            // Dereference checkboxes
            this.checkboxes$ = null;
        }
    }

    // Handle the category group dropdown
    if ($('div#cat_group_container_'+DistrictPicker.targetCountiesGroup).length !== 0) {
        // Event handler to change the tree when the user changes the category group dropdown
        $('#category-group-select #category-group').change(function () {
            DistrictPicker.tree.destroy();
            DistrictPicker.tree = null;
            DistrictPicker.tree = new Tree($('div#cat_group_container_'+$(this).val()+' input'));
        });
    }
    else
    {
        // Remove the category group dropdown's fieldgroup from the DOM
        $('#category-group-select').remove();
    }

    // Build the initial tree
    DistrictPicker.tree = new Tree($('div#cat_group_container_'+DistrictPicker.countiesGroup+' input'));

}(window.DistrictPicker = window.DistrictPicker || {}, jQuery))
