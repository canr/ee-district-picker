<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

require_once(realpath(dirname(__FILE__)."/Logger.php"));
 
// /**
//  * Creates an accessory to auto-populate an entry with predefined content.
//  */
class District_picker_acc
{	
	public $name			= 'District Picker'; //!< Accessory name.
	public $id              = 'district_picker'; //!< Accessory id string.
	public $version			= '0.0.1'; //!< Accessory version number.
	public $description		= 'Allows users to prepopulate entries from templates.'; //!< Description of the accessory's functionality.
	public $sections		= array();


    private $EE; //!< Reference to the main EE object.

    /**
     * Constructor
     */
    function __construct ()
    {
        // Hook EE
        $this->EE =& get_instance();

        // Start the Logger
        $this->logger = new DistrictPickerLogger("district_picker.log");
        $this->logger->Log("=== Constructed District_picker_acc ===");
    }
	
	/**
	 * Set Sections
	 */
	public function set_sections()
	{
        // Populate view data
        try
        {
            $data = array();
            $data['CountiesGroup'] = $this->query_group('Counties');
            $data['TargetCountiesGroup'] = $this->query_group('Target Counties');
        }
        catch (Exception $e)
        {
            $this->logger->Log($e->getMessage());
        }

        // Load view
		$this->sections['District Picker'] = $this->EE->load->view('district_picker', $data, TRUE);
	}

    /**
     * Queries the database for a category group's ID given its name.
     *
     * @param  string $group_name Name of the target category group.
     * @return int The ID of the target category group.
     */
    private function query_group ($group_name)
    {
        // Query
        $result = $this->EE->db
            -> select (
                'group_id'
            )
            -> from ('exp_category_groups')
            -> where ("site_id", $this->EE->config->item('site_id'))
                -> where ("group_name", $group_name)
            -> get() -> result_array();
        
        // No results?
        if (empty($result))
            throw new Exception("No category group named $group_name found.");

        return $result[0]['group_id'];
    }
}
 
/* End of file acc.entry_templates.php */
/* Location: /system/expressionengine/third_party/purge/acc.entry_templates.php */
