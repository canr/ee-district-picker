<style>
<?php require_once(realpath(dirname(__FILE__)."/../css/styles.css")); ?>
</style>

<form id="district-picker">
    <fieldset id="category-group-select">
        <div class="fieldset-box">
            <label class="fieldset-label">Category Group</label>
            <select id="category-group" name="category-group">
                <option value="<?=$CountiesGroup?>">Counties</option>
                <option value="<?=$TargetCountiesGroup?>">Target Counties</option>
            </select>
            <p class="explaination">Choose whether this tool works on the entry's counties or its target counties.</p>
        </div>
    </fieldset>

    <div id="metacheckboxes">
        <fieldset id="statewide" class="district-picker-boxes">
            <div class="fieldset-box">
                <label class="fieldset-label">Statewide</label>
                    <span class="checkbox">
                        <label for="statewide">All</label>
                        <input type="checkbox" name="statewide" class="district-picker statewide" value="Statewide" />
                    </span>
                <p class="explaination">Click this box to select or deselect all counties in the state.</p>
            </div>
        </fieldset>

        <fieldset id="metadistricts" class="district-picker-boxes">
            <div class="fieldset-box">
                <label class="fieldset-label">Peninsula</label>
                <?php foreach( array("Upper Peninsula", "Lower Peninsula") as $metadistrict) { ?>
                    <span class="checkbox">
                        <label for="metadistrict"><?=$metadistrict?></label>
                        <input type="checkbox" name="metadistrict" class="district-picker metadistrict" value="<?=$metadistrict?>" />
                    </span>
                <?php } ?>
                <p class="explaination">Click one of these boxes to select or deselect all counties in that peninsula.</p>
            </div>
        </fieldset>

        <fieldset id="districts" class="district-picker-boxes">
            <div class="fieldset-box">
                <label class="fieldset-label">District</label>
                <?php for ($i = 1; $i < 14; $i++) { ?>
                    <span class="checkbox">
                        <label for="district"><?=$i?></label>
                        <input type="checkbox" name="district" class="district-picker district" value="<?=$i?>" />
                    </span>
                <?php } ?>
                <p class="explaination">Click one of these boxes to select or deselect all counties in that district.</p>
            </div>
        </fieldset>
    </div>
</form>

<script type="text/javascript">
(function (DistrictPicker, $, undefined) {
    DistrictPicker.countiesGroup = <?=$CountiesGroup?>;
    DistrictPicker.targetCountiesGroup = <?=$TargetCountiesGroup?>;
}(window.DistrictPicker = window.DistrictPicker || {}, jQuery))
</script>

<script type="text/javascript">
<? require_once(realpath(dirname(__FILE__)."/../data/district_map.js")); ?>
</script>

<script type="text/javascript">
<? require_once(realpath(dirname(__FILE__)."/../js/district_picker.js")); ?>
</script>
