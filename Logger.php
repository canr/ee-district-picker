<?php

/**
 * Logs events to files and EE's database.
 */
class DistrictPickerLogger
{
    # ###########
    # Member Data
    # ###########
    // Debug
    private $debug = TRUE; /**< Whether debug statements should be written to the log file. */
    private $logfile; /**< File to which debug statements shall be logged. */


    # #########
    # Built-ins
    # #########
    /**
     * Constructor
     *
     * @param string $logfile The name of the file to which log messages are written.
     */
    public function __construct ($logfile)
    {
        // Hook EE instance
        $this->EE =& get_instance();

        // Set log file
        $this->logfile = $logfile;
    }

    # #################
    # Logging Functions
    # #################
    /**
     * Logs a given event string to a standard file.
     *
     * @param mixed $obj The event object to be written to the log file.
     * @return int Returns the number of bytes written to the log file.
     */
    public function Log ($obj)
    {
        // Debug mode?
        if (! $this->debug)
            return 0;

        // Prefix with timestamp
        $output = "[".date("Y-m-d H:i:s")."]  ";

        // Construct output string
        $output .= $this->GenOutput($obj);

        // Construct log file path
        $path = dirname(__FILE__)."/".$this->logfile;

        // Create file if needed
        if (! file_exists($path))
        {
            $touchSuccess = @touch($path);

            if (! $touchSuccess)
            {
                $this->EELog("Unable to create log file at $path.", TRUE);
            }
        }

        // Write output string to file
        $bytesWritten = @file_put_contents($path, $output, FILE_APPEND);

        // Raise exception on failure
        if (! $bytesWritten)
        {
            $this->EELog("Unable to write to log file at $path.", TRUE);
        }

        return $bytesWritten;
    }

    /**
     * Logs a given event string to ExpressionEngine's developer log file.
     *
     * @param mixed $obj The event object to be written to the EE developer log.
     * @param boolean $unique Whether the message should be unique in the developer log. If TRUE and the string is already in the developer log, the log entry will be set to not viewed and its timestamp will be updated.
     */

    public function EELog($obj, $unique = FALSE)
    {
        // Debug mode?
        if (! $this->debug)
            return 0;

        // Load EE logger
        $this->EE->load->library('logger');

        // Construct output string
        $output = $this->GenOutput($obj);

        // Write to developer log
        $this->EE->logger->developer($output, $unique);
    }


    # ######
    # Output
    # ######
    /**
     * Converts an object into a string to be inserted in a log.
     *
     * @param mixed $obj The event object to be written to the log.
     * @return string A string prefixed with a timestamp and formatted to be written to a log.
     */
    private function GenOutput ($obj)
    {
        if ( is_array($obj) OR is_object($obj) )
        {
            // Start an output buffer for print_r()
            ob_start();

            // print_r() the object to generate its output string
            print_r($obj);

            // Append the print_r() output to the output string and close the buffer
            $output = ob_get_clean();
        }
        else
        {
            $output = $obj;
        }

        // Newline
        $output .= "\n";

        return $output;
    }
}


/* End of file Logger.php */
/* Location: /system/expressionengine/third_party/entry_templates/Logger.php */
